# -*- coding: utf-8 -*-

import sys
import os
import re
import aether_sphinx

if not 'READTHEDOCS' in os.environ:
    sys.path.insert(0, os.path.abspath('..'))
sys.path.append(os.path.abspath('./demo/'))

from sphinx.locale import _

from aether_sphinx import __version__

project = u'Aether Sphinx Theme'
slug = re.sub(r'\W+', '-', project.lower())
version = __version__
release = __version__
author = u'Janet Blackquill, Carl Schwan, Dave Snider, Read the Docs, Inc. & contributors'
copyright = author
language = 'en'

extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.autodoc',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
    'aether_sphinx',
]

templates_path = ['_templates']
source_suffix = '.rst'
exclude_patterns = []
locale_dirs = ['locale/']
gettext_compact = False

master_doc = 'index'
suppress_warnings = ['image.nonlocal_uri']
pygments_style = 'default'

intersphinx_mapping = {
    'rtd': ('https://docs.readthedocs.io/en/latest/', None),
    'sphinx': ('http://www.sphinx-doc.org/en/stable/', None),
}

html_theme = 'aether'
html_theme_options = {
    'logo_only': True
}
html_theme_path = ["../.."]
html_logo = "demo/static/logo-wordmark-light.svg"
html_show_sourcelink = True

htmlhelp_basename = slug

latex_documents = [
  ('index', '{0}.tex'.format(slug), project, author, 'manual'),
]

man_pages = [
    ('index', slug, project, [author], 1)
]

texinfo_documents = [
  ('index', slug, project, author, slug, project, 'Miscellaneous'),
]

rst_prolog = """
.. role:: iconred
.. role:: plasmablue
.. role:: noblefir
.. role:: ambientamber
.. role:: intend
"""

rst_epilog = """
.. |devicon| image:: /img/DevIcon.svg
             :width: 32px
             :height: 32px

.. |designicon| image:: /img/DesignerIcon.svg
                :width: 32px
                :height: 32px

.. |touchicon| image:: /img/transform-browse.svg
                :width: 32px
                :height: 32px

.. |desktopicon| image:: /img/computer.svg
             :width: 32px
             :height: 32px

.. |mobileicon| image:: /img/smartphone.svg
             :width: 32px
             :height: 32px


.. |br| raw:: html

   <br />

.. |nbsp| raw:: html

   &#160;

"""

# Extensions to theme docs
def setup(app):
    from sphinx.domains.python import PyField
    from sphinx.util.docfields import Field

    app.add_object_type(
        'confval',
        'confval',
        objname='configuration value',
        indextemplate='pair: %s; configuration value',
        doc_field_types=[
            PyField(
                'type',
                label=_('Type'),
                has_arg=False,
                names=('type',),
                bodyrolename='class'
            ),
            Field(
                'default',
                label=_('Default'),
                has_arg=False,
                names=('default',),
            ),
        ]
    )

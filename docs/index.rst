.. include:: ../README.rst

.. toctree::
   :caption: Theme Documentation
   :maxdepth: 2

   installing
   configuring
   contributing

.. toctree::
   :maxdepth: 1
   :hidden:

   changelog

.. toctree::
    :maxdepth: 2
    :numbered:
    :caption: Demo Documentation

    demo/structure
    demo/demo
    demo/lists_tables
    demo/api

.. toctree::
    :maxdepth: 3
    :numbered:
    :caption: This is an incredibly long caption for a long menu

    demo/long

.. container:: flex

   .. container::

      .. figure:: /img/pixel-align-do.png
         :scale: 80%
         :figclass: do

         :noblefir:`Do.` |br|
         Make sure your icon is aligned 
         to the pixel grid—use grids and guides
         to assist you when designing it.

   .. container::

      .. figure:: /img/pixel-align-dont.png
         :scale: 80%
         :figclass: dont

         :iconred:`Don't.` |br|
         Don't misalign your icon to the pixel
         grid—this makes it look blurry when
         scaled and can make it look wonky.